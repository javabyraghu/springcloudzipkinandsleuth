package in.nareshit.raghu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudAppOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudAppOneApplication.class, args);
	}

}
